#ifndef WRITE_GLTF_H_
#define WRITE_GLTF_H_

#include <string>

#include "class_board.h"

/**
 * @brief WriteGLTFLayer - Writes the specified layer of the board to a binary
 * GLTF file of the same name
 *
 * @param board is a pointer to the main PCB board instance.
 * @param layer_id describes which layer of the board to export.
 * @param out_dir is the path to the directory to write the resulting file in.
 */
void WriteGLTFLayer( const BOARD* board, const PCB_LAYER_ID layer_id, const wxString& out_dir );

/**
 * @brief WriteGLTFVias - Writes the vias of the board to a binary GLTF file
 * named `vias.glb`
 *
 * @param board is a pointer to the main PCB board instance.
 * @param layer_in_front - The generated model will be a cross-section of any
 *        vias that fall between `layer_in_front` and `layer_in_front + 1`
 * @param out_dir is the path to the directory to write the resulting file in.
 */
void WriteGLTFVias( const BOARD* board, const PCB_LAYER_ID layer_in_front, const wxString& out_dir );

#endif
