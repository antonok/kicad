#include <future>
#include <gltf_writer.h>

#include "write_gltf/write_gltf.h"
#include "godot_server.h"

static GODOT_SERVER server;

GLTF_WRITER::GLTF_WRITER( BOARD* board, BOARD_ITEM* boardItem )
    : m_output_dir(wxString("gltf-out")),
      m_layer_id(boardItem -> GetLayer()),
      m_board(board)
{
}

auto GLTF_WRITER::CallWriteGLTFAsync(BOARD* board, 
	BOARD_ITEM* boardItem, wxFileName output_dir)
{
	// boost::async returns a future
	return boost::async(std::launch::async, &GLTF_WRITER::CallWriteGLTF,
		shared_from_this(), board, boardItem, output_dir);
}

void GLTF_WRITER::CallWriteGLTF(BOARD* board, 
	BOARD_ITEM* boardItem, wxFileName output_dir)
{
    WriteGLTF(board, boardItem->GetLayer(), output_dir);
}


GLTF_WRITER::~GLTF_WRITER()
{
}
