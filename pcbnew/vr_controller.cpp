#include <vr_controller.h>

#include <godot_server.h>
#include <write_gltf/write_gltf.h>

#include <thread>
#include <mutex>
#include <future>

VR_CONTROLLER* VR_CONTROLLER::vr_controller = nullptr;
unsigned VR_CONTROLLER::file_id = 0;

VR_CONTROLLER::VR_CONTROLLER()
    :   m_is_active(true),
        m_server(std::make_shared<GODOT_SERVER>("127.0.0.1", 8080)),
        m_lock()
{
    std::cout << "VR_CONTROLLER constructor" << std::endl;

    // kick off server async
    m_handle = std::async(std::launch::async, [this](){
        std::cout << "Running forever" << std::endl;
        m_server->run_forever();
    });
}

VR_CONTROLLER* VR_CONTROLLER::GetInstance() {
    std::cout << "get_instance" << std::endl;
    if (!vr_controller) {
        std::cout << "Creating new VR_CONTROLLER" << std::endl;
        vr_controller = new VR_CONTROLLER;
        std::cout << "Done" << std::endl;
    }
    return vr_controller;
}

void VR_CONTROLLER::Activate(const BOARD* board) {
    m_is_active = true;

    // get directory from server
    boost::optional<std::string> maybe_path = m_server->GetGLTFOutputDir();
    std::cout << "PATH is " << (maybe_path ? *maybe_path : "NONE") << std::endl;

    if (maybe_path) {
        wxString out_dir(*maybe_path);

        // TODO the json stuff could be in the server class
        json board_out = {
            {"type", "load-gltf-layer"},
            {"payload", {}}
        };

        unsigned int layerNum = 0;

        // loop through all layers, write them, and add them to the payload
        for (LAYER_NUM layer = F_Cu; layer <= B_Cu; ++layer) {
            PCB_LAYER_ID id = ToLAYER_ID(layer);
            if (board->IsLayerEnabled(id)) {

                // Write the layer
                WriteGLTFLayer(board, id, out_dir);

                // And the vias
                WriteGLTFVias(board, id, out_dir);

                // TODO get this from WriteGTLFLayer
                wxFileName fname_layer;
                fname_layer.Assign(out_dir, BOARD::GetStandardLayerName(id), wxString("glb"));

                // TODO get this from WriteGTLFLayer
                wxFileName fname_vias;
                fname_vias.Assign(out_dir, wxString("vias-") + BOARD::GetStandardLayerName(id), 
                    wxString("glb"));

                json layer_json = {
                    {"layerPath", fname_layer.GetFullPath().ToStdString()},
                    {"throughHoleMaskPath", nullptr},
                    {"belowViasPath", fname_vias.GetFullPath().ToStdString()},
                    {"displayName", BOARD::GetStandardLayerName(id).ToStdString()}
                };

                board_out["payload"][std::to_string(layerNum)] = layer_json;

                layerNum++;
            }
        }

        // The payload is built, send it
        m_server->Send(board_out);
    } else {
        std::cerr << "No output directory" << std::endl;
    }
}

void VR_CONTROLLER::Deactivate() {
    // TODO send end message to GODOT
}

void VR_CONTROLLER::PushToGodot(BOARD* board, PCB_LAYER_ID layer_id) {
    if (m_is_active) {

        // get directory from server
        boost::optional<std::string> maybe_path = m_server->GetGLTFOutputDir();
        std::cout << "PATH is " << (maybe_path ? *maybe_path : "NONE") << std::endl;

        if (maybe_path) {

            // launch in separate thread
            std::async(std::launch::async, [this, board, layer_id, 
                    maybe_path]() {
                std::lock_guard<std::mutex> lock(m_lock);

                // write the layer TODO(mike) suffixes
                WriteGLTFLayer(board, layer_id, wxString(*maybe_path));

                // write the vias TODO(mike) suffixes
                WriteGLTFVias(board, layer_id, wxString(*maybe_path));

                // when its done, tell godot
                m_server->SendUpdate(layer_id, *maybe_path);
            });
        } else {
            std::cerr << "No output directory" << std::endl;
        }
    }
}
