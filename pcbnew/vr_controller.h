#ifndef VR_CONTROLLER_H
#define VR_CONTROLLER_H

#include <godot_server.h>
#include <write_gltf/write_gltf.h>

#include <thread>
#include <mutex>
#include <future>

class VR_CONTROLLER {
private:

    static VR_CONTROLLER* vr_controller; // singleton
    static unsigned file_id; // increments every write

    bool m_is_active; // true if the VR window is currently open
    std::shared_ptr<GODOT_SERVER> m_server; // server - always runs currently b/c singleton
    std::mutex m_lock; // lock for async calls
    std::future<void> m_handle; // handle for server->run async call

    // private constructor
    VR_CONTROLLER();

public:

    /**
     * @brief Get the singleton object
     * 
     * @return  The singleton instance
     */
    static VR_CONTROLLER* GetInstance();

    /**
     * @brief Start the VR controller
     * 
     * This will set the controller to it's active state, and will send a
     * load-gltf-layer command with all current board layers.
     * 
     * @param board The board to update
     */
    void Activate(const BOARD* board);
    
    /**
     * @brief Stop the VR controller
     * 
     * This will stop sending any messages to Godot, after sending the `end` 
     * message.
     */
    void Deactivate();

    /**
     * @brief Push a single layer to GODOT using the update-gltf-layer
     * command.
     * 
     * @note This message type is not implemented on the Godot side yet.
     * 
     * @param board     The board
     * @param layer_id  The layer to update 
     * @return  
     */
    void PushToGodot(BOARD* board, PCB_LAYER_ID layer_id);
};

#endif // VR_CONTROLLER_H
