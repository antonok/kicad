#ifndef GLTF_WRITER_H_
#define GLTF_WRITER_H_

#include <string>

#include "class_board.h"


/**
 * @brief GLTF_WRITER - Wrapper for WriteGLTF class that determines the correct layer
 * and handles the output directory to call the function with.
 */

class GLTF_WRITER : public std::enable_shared_from_this<GLTF_WRITER>
{
public:
    GLTF_WRITER();

    ~GLTF_WRITER();

	std::future<void> CallWriteGLTFAsync(BOARD* board, BOARD_ITEM* boardItem, 
		wxFileName output_dir);

	void CallWriteGLTF(BOARD* board, BOARD_ITEM* boardItem, 
		wxFileName output_dir);
private:
    wxString m_output_dir;
    PCB_LAYER_ID m_layer_id;
    BOARD* m_board;
};

#endif
