/*#define PCBNEW
#include <convert_to_biu.h>
#undef PCBNEW*/
constexpr double IU_PER_MM = 1e6;
constexpr double MM_PER_IU = ( 1 / IU_PER_MM );


/// Convert mm to internal units (iu).
constexpr inline int Millimeter2iu( double mm )
{
    return (int) ( mm < 0 ? mm * IU_PER_MM - 0.5 : mm * IU_PER_MM + 0.5 );
}

/// Convert mm to internal units (iu).
constexpr inline double Iu2Millimeter( int iu )
{
    return iu / IU_PER_MM;
}

constexpr int ARC_LOW_DEF = Millimeter2iu( 0.02 );
constexpr int ARC_HIGH_DEF = Millimeter2iu( 0.005 );

#include "class_board.h"
#include "class_dimension.h"
#include "class_drawsegment.h"
#include "class_module.h"
#include <class_pcb_text.h>
#include "geom3d/clayer_triangles.h"

#include <plugins/3dapi/xv3d_types.h>
#include <geometry/geometry_utils.h>
#include "../3d-viewer/3d_rendering/3d_render_raytracing/accelerators/ccontainer2d.h" //TODO
#include "../3d-viewer/3d_rendering/3d_render_raytracing/shapes2D/cfilledcircle2d.h"
#include "../3d-viewer/3d_rendering/3d_render_raytracing/shapes2D/croundsegment2d.h"
#include "../3d-viewer/3d_rendering/3d_render_raytracing/shapes2D/cpolygon4pts2d.h"
#include "../3d-viewer/3d_rendering/3d_render_raytracing/shapes2D/ctriangle2d.h"
#include "../3d-viewer/3d_rendering/3d_render_raytracing/shapes2D/cring2d.h"

#include "convert_basic_shapes_to_polygon.h"

#include "geom3d/geom3d.h"

#define SIZE_OF_CIRCLE_TEXTURE 1024

// TODO determine if this needs to follow logic from CINFO3D_VISU (prob not, we
// want to maintain consistent scale)
const double biu_to_3d_units = 1e-7;

#define COPPER_THICKNESS KiROUND( 0.035 * IU_PER_MM )   // for 35 um
const float copperThickness3DU = COPPER_THICKNESS * biu_to_3d_units;

unsigned int GetNrSegmentsCircleBIU( int aDiameterBIU )
{
    wxASSERT( aDiameterBIU > 0 );

    // Require at least 3 segments for a circle
    return std::max( GetArcToSegmentCount( aDiameterBIU / 2, ARC_HIGH_DEF, 360.0 ), 3 );
}

double GetCircleCorrectionFactor( int aNrSides )
{
    wxASSERT( aNrSides >= 3 );

    return GetCircletoPolyCorrectionFactor( aNrSides );
}

unsigned int GetStatsNrVias( const BOARD* board, float* med_via_hole_diameter )
{
    unsigned int nr_vias = 0;
    *med_via_hole_diameter = 0;

    for( const TRACK* track = board->m_Track; track; track = track->Next() )
    {
        if( track->Type() == PCB_VIA_T )
        {
            const VIA *via = static_cast< const VIA*>( track );
            nr_vias++;
            *med_via_hole_diameter += via->GetDrillValue() * biu_to_3d_units;
        }
    }

    *med_via_hole_diameter /= (float)nr_vias;

    return nr_vias;
}

//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void generate_ring_contour( const SFVEC2F &aCenter,
                            float aInnerRadius,
                            float aOuterRadius,
                            unsigned int aNr_sides_per_circle,
                            std::vector< SFVEC2F > &aInnerContourResult,
                            std::vector< SFVEC2F > &aOuterContourResult,
                            bool aInvertOrder )
{
    aInnerContourResult.clear();
    aInnerContourResult.reserve( aNr_sides_per_circle + 2 );

    aOuterContourResult.clear();
    aOuterContourResult.reserve( aNr_sides_per_circle + 2 );

    const int delta = 3600 / aNr_sides_per_circle;

    for( int ii = 0; ii < 3600; ii += delta )
    {
        float angle = (float)( aInvertOrder ? ( 3600 - ii ) : ii )
                            * 2.0f * glm::pi<float>() / 3600.0f;
        const SFVEC2F rotatedDir = SFVEC2F( cos( angle ), sin( angle ) );

        aInnerContourResult.push_back( SFVEC2F( aCenter.x + rotatedDir.x * aInnerRadius,
                                                aCenter.y + rotatedDir.y * aInnerRadius ) );

        aOuterContourResult.push_back( SFVEC2F( aCenter.x + rotatedDir.x * aOuterRadius,
                                                aCenter.y + rotatedDir.y * aOuterRadius ) );
    }

    aInnerContourResult.push_back( aInnerContourResult[0] );
    aOuterContourResult.push_back( aOuterContourResult[0] );

    wxASSERT( aInnerContourResult.size() == aOuterContourResult.size() );
}

//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void generate_cylinder( const SFVEC2F &aCenter,
                        float aInnerRadius,
                        float aOuterRadius,
                        float aZtop,
                        float aZbot,
                        unsigned int aNr_sides_per_circle,
                        CLAYER_TRIANGLES *aDstLayer )
{
    std::vector< SFVEC2F > innerContour;
    std::vector< SFVEC2F > outerContour;

    generate_ring_contour( aCenter,
                           aInnerRadius,
                           aOuterRadius,
                           aNr_sides_per_circle,
                           innerContour,
                           outerContour,
                           false );

    for( unsigned int i = 0; i < ( innerContour.size() - 1 ); ++i )
    {
        const SFVEC2F &vi0 = innerContour[i + 0];
        const SFVEC2F &vi1 = innerContour[i + 1];
        const SFVEC2F &vo0 = outerContour[i + 0];
        const SFVEC2F &vo1 = outerContour[i + 1];

        aDstLayer->m_layer_top_triangles->AddQuad( SFVEC3F( vi1.x, vi1.y, aZtop ),
                                                   SFVEC3F( vi0.x, vi0.y, aZtop ),
                                                   SFVEC3F( vo0.x, vo0.y, aZtop ),
                                                   SFVEC3F( vo1.x, vo1.y, aZtop ) );

        aDstLayer->m_layer_bot_triangles->AddQuad( SFVEC3F( vi1.x, vi1.y, aZbot ),
                                                   SFVEC3F( vo1.x, vo1.y, aZbot ),
                                                   SFVEC3F( vo0.x, vo0.y, aZbot ),
                                                   SFVEC3F( vi0.x, vi0.y, aZbot ) );
    }

    aDstLayer->AddToMiddleContourns( outerContour, aZbot, aZtop, true );
    aDstLayer->AddToMiddleContourns( innerContour, aZbot, aZtop, false );
}

//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_triangle_top_bot( CLAYER_TRIANGLES *aDst,
                           const SFVEC2F &v0,
                           const SFVEC2F &v1,
                           const SFVEC2F &v2,
                           float top,
                           float bot )
{
    aDst->m_layer_bot_triangles->AddTriangle( SFVEC3F( v0.x, v0.y, bot ),
                                              SFVEC3F( v1.x, v1.y, bot ),
                                              SFVEC3F( v2.x, v2.y, bot ) );

    aDst->m_layer_top_triangles->AddTriangle( SFVEC3F( v2.x, v2.y, top ),
                                              SFVEC3F( v1.x, v1.y, top ),
                                              SFVEC3F( v0.x, v0.y, top ) );
}

CLAYER_TRIANGLES* generate_3D_Vias_and_Pads( const BOARD* board, const PCB_LAYER_ID layer_in_front )
{
    float med_via_hole_diameter = 0;
    const unsigned int nr_vias = GetStatsNrVias( board, &med_via_hole_diameter );

    if (med_via_hole_diameter < 1) med_via_hole_diameter = 1;

    SHAPE_POLY_SET tht_outer_holes_poly; // Stores the outer poly of the copper holes (the pad)
    SHAPE_POLY_SET tht_inner_holes_poly; // Stores the inner poly of the copper holes (the hole)

    CCONTAINER2D holesContainer;

    tht_outer_holes_poly.RemoveAllContours();
    tht_inner_holes_poly.RemoveAllContours();

    if (layer_in_front != B_Cu) {
        // Insert pads holes (vertical cylinders)
        for( const MODULE* module = board->m_Modules; module; module = module->Next() )
        {
            for( const D_PAD* pad = module->PadsList(); pad; pad = pad->Next() )
            {
                if( pad->GetAttribute() != PAD_ATTRIB_HOLE_NOT_PLATED )
                {
                    const wxSize drillsize = pad->GetDrillSize();
                    const bool   hasHole   = drillsize.x && drillsize.y;

                    if( !hasHole )
                        continue;

                    // we use the hole diameter to calculate the seg count.
                    // for round holes, drillsize.x == drillsize.y
                    // for slots, the diameter is the smaller of (drillsize.x, drillsize.y)
                    int    radius = std::min( drillsize.x, drillsize.y ) / 2 + COPPER_THICKNESS;
                    int    nrSegments = GetNrSegmentsCircleBIU( radius * 2 );
                    double correctionFactor = GetCircleCorrectionFactor( nrSegments );
                    int    correction = radius * ( correctionFactor - 1 );

                    pad->BuildPadDrillShapePolygon( tht_outer_holes_poly,
                                                    COPPER_THICKNESS + correction,
                                                    nrSegments );

                    pad->BuildPadDrillShapePolygon( tht_inner_holes_poly,
                                                    correction,
                                                    nrSegments );
                }
            }
        }

        // Subtract the holes
        tht_outer_holes_poly.BooleanSubtract( tht_inner_holes_poly, SHAPE_POLY_SET::PM_FAST );

        Convert_shape_line_polygon_to_triangles( tht_outer_holes_poly,
                                                 holesContainer,
                                                 biu_to_3d_units,
                                                 (const BOARD_ITEM &)*board );
    }

    const LIST_OBJECT2D &listHolesObject2d = holesContainer.GetList();

    const unsigned int nr_holes = nr_vias + listHolesObject2d.size();

    const unsigned int reserve_nr_triangles_estimation =
            GetNrSegmentsCircleBIU( med_via_hole_diameter + 0.1 ) * // TODO + 0.1 is a hack in case there are 0 vias
            8 *
            nr_holes;

    CLAYER_TRIANGLES* layerTriangles = new CLAYER_TRIANGLES( reserve_nr_triangles_estimation );

    if( nr_vias > 0 )
    {
        const float thickness = copperThickness3DU;

        // Insert plated vertical holes inside the board
        // /////////////////////////////////////////////////////////////////////////

        // Insert vias holes (vertical cylinders)
        for( const TRACK* track = board->m_Track;
             track;
             track = track->Next() )
        {
            if( track->Type() == PCB_VIA_T )
            {
                const VIA *via = static_cast<const VIA*>(track);

                PCB_LAYER_ID top_layer, bottom_layer;
                via->LayerPair( &top_layer, &bottom_layer );

                if( top_layer <= layer_in_front && bottom_layer > layer_in_front) {
                    const float  holediameter = via->GetDrillValue() * biu_to_3d_units;
                    const int    nrSegments = GetNrSegmentsCircleBIU( via->GetDrillValue() );
                    const double correctionFactor = GetCircleCorrectionFactor( nrSegments );
                    const float  hole_inner_radius = ( holediameter / 2.0f ) * correctionFactor;

                    const SFVEC2F via_center(  via->GetStart().x * biu_to_3d_units,
                                              -via->GetStart().y * biu_to_3d_units );

                    generate_cylinder( via_center,
                                       hole_inner_radius,
                                       hole_inner_radius + thickness,
                                       0.5,
                                       -0.5,
                                       nrSegments,
                                       layerTriangles );
                }
            }
        }
    }

    if( layer_in_front != B_Cu && listHolesObject2d.size() > 0 )
    {
        // Convert the list of objects(triangles) to triangle layer structure
        for( LIST_OBJECT2D::const_iterator itemOnLayer = listHolesObject2d.begin();
             itemOnLayer != listHolesObject2d.end();
             ++itemOnLayer )
        {
            const COBJECT2D *object2d_A = static_cast<const COBJECT2D *>(*itemOnLayer);

            wxASSERT( object2d_A->GetObjectType() == OBJ2D_TRIANGLE );

            const CTRIANGLE2D *tri = (const CTRIANGLE2D *)object2d_A;

            const SFVEC2F &v1 = tri->GetP1();
            const SFVEC2F &v2 = tri->GetP2();
            const SFVEC2F &v3 = tri->GetP3();

            add_triangle_top_bot( layerTriangles, v1, v2, v3, 0.5, -0.5 );
        }

        wxASSERT( tht_outer_holes_poly.OutlineCount() > 0 );

        if( tht_outer_holes_poly.OutlineCount() > 0 )
        {
            layerTriangles->AddToMiddleContourns( tht_outer_holes_poly,
                                                  -0.5, 0.5,
                                                  biu_to_3d_units,
                                                  false );
        }
    }

    return layerTriangles;
}

COBJECT2D* createNewTrack( const TRACK* aTrack,
                           int aClearanceValue )
{
    SFVEC2F start3DU(  aTrack->GetStart().x * biu_to_3d_units,
                      -aTrack->GetStart().y * biu_to_3d_units ); // y coord is inverted

    switch( aTrack->Type() )
    {
    case PCB_VIA_T:
    {
        const float radius = ( ( aTrack->GetWidth() / 2 ) + aClearanceValue ) * biu_to_3d_units;

        return new CFILLEDCIRCLE2D( start3DU, radius, *aTrack );
    }
        break;

    default:
    {
        wxASSERT( aTrack->Type() == PCB_TRACE_T );

        SFVEC2F end3DU (  aTrack->GetEnd().x * biu_to_3d_units,
                         -aTrack->GetEnd().y * biu_to_3d_units );

        // Cannot add segments that have the same start and end point
        if( Is_segment_a_circle( start3DU, end3DU ) )
        {
            const float radius = ((aTrack->GetWidth() / 2) + aClearanceValue) * biu_to_3d_units;

            return new CFILLEDCIRCLE2D( start3DU, radius, *aTrack );
        }
        else
        {
            const float width = (aTrack->GetWidth() + 2 * aClearanceValue ) * biu_to_3d_units;

            return new CROUNDSEGMENT2D( start3DU, end3DU, width, *aTrack );
        }
    }
        break;
    }

    return NULL;
}

//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_object_to_triangle_layer( const CFILLEDCIRCLE2D * aFilledCircle,
                                   CLAYER_TRIANGLES *aDstLayer,
                                   float aZtop,
                                   float aZbot )
{
    const SFVEC2F &center = aFilledCircle->GetCenter();
    const float radius = aFilledCircle->GetRadius() *
                         2.0f; // Double because the render triangle

    // This is a small adjustment to the circle texture
    const float texture_factor = (8.0f / (float)SIZE_OF_CIRCLE_TEXTURE) + 1.0f;
    const float f = (sqrtf(2.0f) / 2.0f) * radius * texture_factor;

    // Top and Bot segments ends are just triangle semi-circles, so need to add
    // it in duplicated
    aDstLayer->m_layer_top_segment_ends->AddTriangle( SFVEC3F( center.x + f, center.y, aZtop ),
                                                      SFVEC3F( center.x - f, center.y, aZtop ),
                                                      SFVEC3F( center.x,
                                                               center.y - f, aZtop ) );

    aDstLayer->m_layer_top_segment_ends->AddTriangle( SFVEC3F( center.x - f, center.y, aZtop ),
                                                      SFVEC3F( center.x + f, center.y, aZtop ),
                                                      SFVEC3F( center.x,
                                                               center.y + f, aZtop ) );

    aDstLayer->m_layer_bot_segment_ends->AddTriangle( SFVEC3F( center.x - f, center.y, aZbot ),
                                                      SFVEC3F( center.x + f, center.y, aZbot ),
                                                      SFVEC3F( center.x,
                                                               center.y - f, aZbot ) );

    aDstLayer->m_layer_bot_segment_ends->AddTriangle( SFVEC3F( center.x + f, center.y, aZbot ),
                                                      SFVEC3F( center.x - f, center.y, aZbot ),
                                                      SFVEC3F( center.x,
                                                               center.y + f, aZbot ) );
}


//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_object_to_triangle_layer( const CPOLYGON4PTS2D * aPoly,
                                   CLAYER_TRIANGLES *aDstLayer,
                                   float aZtop,
                                   float aZbot )
{
    const SFVEC2F &v0 = aPoly->GetV0();
    const SFVEC2F &v1 = aPoly->GetV1();
    const SFVEC2F &v2 = aPoly->GetV2();
    const SFVEC2F &v3 = aPoly->GetV3();

    add_triangle_top_bot( aDstLayer, v0, v2, v1, aZtop, aZbot );
    add_triangle_top_bot( aDstLayer, v2, v0, v3, aZtop, aZbot );
}

//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_object_to_triangle_layer( const CRING2D * aRing,
                                   CLAYER_TRIANGLES *aDstLayer,
                                   float aZtop,
                                   float aZbot )
{
    const SFVEC2F &center = aRing->GetCenter();
    const float inner = aRing->GetInnerRadius();
    const float outer = aRing->GetOuterRadius();

    std::vector< SFVEC2F > innerContour;
    std::vector< SFVEC2F > outerContour;

    generate_ring_contour( center,
                           inner,
                           outer,
                           GetNrSegmentsCircleBIU( outer * 2.0f ),
                           innerContour,
                           outerContour,
                           false );

    // This will add the top and bot quads that will form the approximated ring

    for( unsigned int i = 0; i < ( innerContour.size() - 1 ); ++i )
    {
        const SFVEC2F &vi0 = innerContour[i + 0];
        const SFVEC2F &vi1 = innerContour[i + 1];
        const SFVEC2F &vo0 = outerContour[i + 0];
        const SFVEC2F &vo1 = outerContour[i + 1];

        aDstLayer->m_layer_top_triangles->AddQuad( SFVEC3F( vi1.x, vi1.y, aZtop ),
                                                   SFVEC3F( vi0.x, vi0.y, aZtop ),
                                                   SFVEC3F( vo0.x, vo0.y, aZtop ),
                                                   SFVEC3F( vo1.x, vo1.y, aZtop ) );

        aDstLayer->m_layer_bot_triangles->AddQuad( SFVEC3F( vi1.x, vi1.y, aZbot ),
                                                   SFVEC3F( vo1.x, vo1.y, aZbot ),
                                                   SFVEC3F( vo0.x, vo0.y, aZbot ),
                                                   SFVEC3F( vi0.x, vi0.y, aZbot ) );
    }
}


//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_object_to_triangle_layer( const CTRIANGLE2D * aTri,
                                   CLAYER_TRIANGLES *aDstLayer,
                                   float aZtop,
                                   float aZbot )
{
    const SFVEC2F &v1 = aTri->GetP1();
    const SFVEC2F &v2 = aTri->GetP2();
    const SFVEC2F &v3 = aTri->GetP3();

    add_triangle_top_bot( aDstLayer, v1, v2, v3, aZtop, aZbot );
}


//TODO deduplicate with c3d_render_createscene_ogl_legacy.cpp
void add_object_to_triangle_layer( const CROUNDSEGMENT2D * aSeg,
                                   CLAYER_TRIANGLES *aDstLayer,
                                   float aZtop,
                                   float aZbot )
{
    const SFVEC2F leftStart   = aSeg->GetLeftStar();
    const SFVEC2F leftEnd     = aSeg->GetLeftEnd();
    const SFVEC2F leftDir     = aSeg->GetLeftDir();

    const SFVEC2F rightStart  = aSeg->GetRightStar();
    const SFVEC2F rightEnd    = aSeg->GetRightEnd();
    const SFVEC2F rightDir    = aSeg->GetRightDir();
    const float   radius      = aSeg->GetRadius();

    const SFVEC2F start       = aSeg->GetStart();
    const SFVEC2F end         = aSeg->GetEnd();

    const float texture_factor = (12.0f / (float)SIZE_OF_CIRCLE_TEXTURE) + 1.0f;
    const float texture_factorF= ( 6.0f / (float)SIZE_OF_CIRCLE_TEXTURE) + 1.0f;

    const float radius_of_the_square = sqrtf( aSeg->GetRadiusSquared() * 2.0f );
    const float radius_triangle_factor = (radius_of_the_square - radius) / radius;

    const SFVEC2F factorS = SFVEC2F( -rightDir.y * radius * radius_triangle_factor,
                                      rightDir.x * radius * radius_triangle_factor );

    const SFVEC2F factorE = SFVEC2F( -leftDir.y  * radius * radius_triangle_factor,
                                      leftDir.x  * radius * radius_triangle_factor );

    // Top end segment triangles (semi-circles)
    aDstLayer->m_layer_top_segment_ends->AddTriangle(
                SFVEC3F( rightEnd.x  + texture_factor * factorS.x,
                         rightEnd.y  + texture_factor * factorS.y,
                         aZtop ),
                SFVEC3F( leftStart.x + texture_factor * factorE.x,
                         leftStart.y + texture_factor * factorE.y,
                         aZtop ),
                SFVEC3F( start.x - texture_factorF * leftDir.x * radius * sqrtf( 2.0f ),
                         start.y - texture_factorF * leftDir.y * radius * sqrtf( 2.0f ),
                         aZtop ) );

    aDstLayer->m_layer_top_segment_ends->AddTriangle(
                SFVEC3F( leftEnd.x    + texture_factor * factorE.x,
                         leftEnd.y    + texture_factor * factorE.y, aZtop ),
                SFVEC3F( rightStart.x + texture_factor * factorS.x,
                         rightStart.y + texture_factor * factorS.y, aZtop ),
                SFVEC3F( end.x - texture_factorF * rightDir.x * radius * sqrtf( 2.0f ),
                         end.y - texture_factorF * rightDir.y * radius * sqrtf( 2.0f ),
                         aZtop ) );

    // Bot end segment triangles (semi-circles)
    aDstLayer->m_layer_bot_segment_ends->AddTriangle(
                SFVEC3F( leftStart.x + texture_factor * factorE.x,
                         leftStart.y + texture_factor * factorE.y,
                         aZbot ),
                SFVEC3F( rightEnd.x  + texture_factor * factorS.x,
                         rightEnd.y  + texture_factor * factorS.y,
                         aZbot ),
                SFVEC3F( start.x - texture_factorF * leftDir.x * radius * sqrtf( 2.0f ),
                         start.y - texture_factorF * leftDir.y * radius * sqrtf( 2.0f ),
                         aZbot ) );

    aDstLayer->m_layer_bot_segment_ends->AddTriangle(
                SFVEC3F( rightStart.x + texture_factor * factorS.x,
                         rightStart.y + texture_factor * factorS.y, aZbot ),
                SFVEC3F( leftEnd.x    + texture_factor * factorE.x,
                         leftEnd.y    + texture_factor * factorE.y, aZbot ),
                SFVEC3F( end.x - texture_factorF * rightDir.x * radius * sqrtf( 2.0f ),
                         end.y - texture_factorF * rightDir.y * radius * sqrtf( 2.0f ),
                         aZbot ) );

    // Segment top and bot planes
    aDstLayer->m_layer_top_triangles->AddQuad(
                SFVEC3F( rightEnd.x,   rightEnd.y,   aZtop ),
                SFVEC3F( rightStart.x, rightStart.y, aZtop ),
                SFVEC3F( leftEnd.x,    leftEnd.y,    aZtop ),
                SFVEC3F( leftStart.x,  leftStart.y,  aZtop ) );

    aDstLayer->m_layer_bot_triangles->AddQuad(
                SFVEC3F( rightEnd.x,   rightEnd.y,   aZbot ),
                SFVEC3F( leftStart.x,  leftStart.y,  aZbot ),
                SFVEC3F( leftEnd.x,    leftEnd.y,    aZbot ),
                SFVEC3F( rightStart.x, rightStart.y, aZbot ) );
}

void createNewPadWithClearance( const D_PAD* aPad,
                                CGENERICCONTAINER2D *aDstContainer,
                                wxSize aClearanceValue )
{
    // note: for most of shapes, aClearanceValue.x = aClearanceValue.y
    // only rectangular and oval shapes can have different values
    // when drawn on the solder paste layer, because we can have a margin that is a
    // percent of pad size
    const int dx = (aPad->GetSize().x / 2) + aClearanceValue.x;
    const int dy = (aPad->GetSize().y / 2) + aClearanceValue.y;

    wxPoint PadShapePos = aPad->ShapePos(); // Note: for pad having a shape offset,
                                            // the pad position is NOT the shape position

    switch( aPad->GetShape() )
    {
    case PAD_SHAPE_CIRCLE:
    {
        const float radius = dx * biu_to_3d_units;

        const SFVEC2F center(  PadShapePos.x * biu_to_3d_units,
                              -PadShapePos.y * biu_to_3d_units );

        aDstContainer->Add( new CFILLEDCIRCLE2D( center, radius, *aPad ) );
    }
    break;

    case PAD_SHAPE_OVAL:
    {
        if( dx == dy )
        {
            // The segment object cannot store start and end the same position,
            // so add a circle instead
            const float radius = dx * biu_to_3d_units;

            const SFVEC2F center(  PadShapePos.x * biu_to_3d_units,
                                  -PadShapePos.y * biu_to_3d_units );

            aDstContainer->Add( new CFILLEDCIRCLE2D( center, radius, *aPad ) );
        }
        else
        {
            // An oval pad has the same shape as a segment with rounded ends

            int iwidth;
            wxPoint shape_offset = wxPoint( 0, 0 );

            if( dy > dx )   // Oval pad X/Y ratio for choosing translation axis
            {
                shape_offset.y = dy - dx;
                iwidth = dx * 2;
            }
            else    //if( dy < dx )
            {
                shape_offset.x = dy - dx;
                iwidth = dy * 2;
            }

            RotatePoint( &shape_offset, aPad->GetOrientation() );

            const wxPoint start = PadShapePos - shape_offset;
            const wxPoint end   = PadShapePos + shape_offset;

            const SFVEC2F start3DU(  start.x * biu_to_3d_units, -start.y * biu_to_3d_units );
            const SFVEC2F end3DU  (    end.x * biu_to_3d_units,   -end.y * biu_to_3d_units );

             // Cannot add segments that have the same start and end point
            if( Is_segment_a_circle( start3DU, end3DU ) )
            {
                aDstContainer->Add( new CFILLEDCIRCLE2D( start3DU,
                                                         (iwidth / 2) * biu_to_3d_units,
                                                         *aPad ) );
            }
            else
            {
                aDstContainer->Add( new CROUNDSEGMENT2D( start3DU, end3DU,
                                                         iwidth * biu_to_3d_units,
                                                         *aPad ) );
            }
        }
    }
    break;

    case PAD_SHAPE_TRAPEZOID:
    case PAD_SHAPE_RECT:
    {
        // see pcbnew/board_items_to_polygon_shape_transform.cpp

        wxPoint corners[4];
        aPad->BuildPadPolygon( corners, wxSize( 0, 0), aPad->GetOrientation() );

        SFVEC2F corners3DU[4];

        // Note: for pad having a shape offset,
        // the pad position is NOT the shape position
        for( unsigned int ii = 0; ii < 4; ++ii )
        {
            corners[ii] += aPad->ShapePos();          // Shift origin to position

            corners3DU[ii] = SFVEC2F( corners[ii].x * biu_to_3d_units,
                                      -corners[ii].y * biu_to_3d_units );
        }


        // Learn more at:
        // https://lists.launchpad.net/kicad-developers/msg18729.html

        // Add the PAD polygon
        aDstContainer->Add( new CPOLYGON4PTS2D( corners3DU[0],
                                                corners3DU[1],
                                                corners3DU[2],
                                                corners3DU[3],
                                                *aPad ) );

        // Add the PAD contours
        // Round segments cannot have 0-length elements, so we approximate them
        // as a small circle
        for( int i = 1; i <= 4; i++ )
        {
            if( Is_segment_a_circle( corners3DU[i - 1], corners3DU[i & 3] ) )
            {
                aDstContainer->Add( new CFILLEDCIRCLE2D( corners3DU[i - 1],
                                        aClearanceValue.x * biu_to_3d_units,
                                        *aPad ) );
            }
            else
            {
                aDstContainer->Add( new CROUNDSEGMENT2D( corners3DU[i - 1],
                                                         corners3DU[i & 3],
                                                         aClearanceValue.x * 2.0f * biu_to_3d_units,
                                                         *aPad ) );
            }
        }
    }
    break;

    case PAD_SHAPE_ROUNDRECT:
    {
        wxSize shapesize( aPad->GetSize() );
        shapesize.x += aClearanceValue.x * 2;
        shapesize.y += aClearanceValue.y * 2;

        int rounding_radius = aPad->GetRoundRectCornerRadius( shapesize );

        wxPoint corners[4];

        GetRoundRectCornerCenters( corners,
                                   rounding_radius,
                                   PadShapePos,
                                   shapesize,
                                   aPad->GetOrientation() );

        SFVEC2F corners3DU[4];

        for( unsigned int ii = 0; ii < 4; ++ii )
            corners3DU[ii] = SFVEC2F( corners[ii].x * biu_to_3d_units,
                                     -corners[ii].y * biu_to_3d_units );

        // Add the PAD polygon (For some reason the corners need
        // to be inverted to display with the correctly orientation)
        aDstContainer->Add( new CPOLYGON4PTS2D( corners3DU[0],
                                                corners3DU[3],
                                                corners3DU[2],
                                                corners3DU[1],
                                                *aPad ) );

        // Add the PAD contours
        // Round segments cannot have 0-length elements, so we approximate them
        // as a small circle
        for( int i = 1; i <= 4; i++ )
        {
            if( Is_segment_a_circle( corners3DU[i - 1], corners3DU[i & 3] ) )
            {
                aDstContainer->Add( new CFILLEDCIRCLE2D( corners3DU[i - 1],
                                        rounding_radius * biu_to_3d_units,
                                        *aPad ) );
            }
            else
            {
                aDstContainer->Add( new CROUNDSEGMENT2D( corners3DU[i - 1],
                                        corners3DU[i & 3],
                                        rounding_radius * 2.0f * biu_to_3d_units,
                                        *aPad ) );
            }
        }
    }
    break;

    case PAD_SHAPE_CUSTOM:
    {
        SHAPE_POLY_SET polyList;     // Will contain the pad outlines in board coordinates
        polyList.Append( aPad->GetCustomShapeAsPolygon() );
        aPad->CustomShapeAsPolygonToBoardPosition( &polyList, aPad->ShapePos(), aPad->GetOrientation() );

        if( aClearanceValue.x )
            polyList.Inflate( aClearanceValue.x, 32 );

        // Add the PAD polygon
        Convert_shape_line_polygon_to_triangles( polyList, *aDstContainer, biu_to_3d_units, *aPad );

    }
        break;
    }
}

void createNewPad( const D_PAD* aPad,
                   CGENERICCONTAINER2D *aDstContainer,
                   wxSize aInflateValue )
{
    switch( aPad->GetShape() )
    {
    case PAD_SHAPE_CIRCLE:
    case PAD_SHAPE_OVAL:
    case PAD_SHAPE_ROUNDRECT:
    case PAD_SHAPE_CUSTOM:
        createNewPadWithClearance( aPad, aDstContainer, aInflateValue );
        break;

    case PAD_SHAPE_TRAPEZOID:
    case PAD_SHAPE_RECT:
        wxPoint corners[4];
        aPad->BuildPadPolygon( corners, aInflateValue, aPad->GetOrientation() );

        // Note: for pad having a shape offset,
        // the pad position is NOT the shape position
        for( unsigned int ii = 0; ii < 4; ++ii )
            corners[ii] += aPad->ShapePos(); // Shift origin to position

        aDstContainer->Add( new CPOLYGON4PTS2D(
                                SFVEC2F( corners[0].x * biu_to_3d_units,
                                        -corners[0].y * biu_to_3d_units ),
                                SFVEC2F( corners[1].x * biu_to_3d_units,
                                        -corners[1].y * biu_to_3d_units ),
                                SFVEC2F( corners[2].x * biu_to_3d_units,
                                        -corners[2].y * biu_to_3d_units ),
                                SFVEC2F( corners[3].x * biu_to_3d_units,
                                        -corners[3].y * biu_to_3d_units ),
                                *aPad ) );

        break;
    }
}

void AddPadsShapesWithClearanceToContainer( const MODULE* aModule,
                                            CGENERICCONTAINER2D *aDstContainer,
                                            PCB_LAYER_ID aLayerId,
                                            int aInflateValue,
                                            bool aSkipNPTHPadsWihNoCopper )
{
    const D_PAD* pad = aModule->PadsList();

    wxSize margin;

    for( ; pad != NULL; pad = pad->Next() )
    {
        if( !pad->IsOnLayer( aLayerId ) )
            continue;

        // NPTH pads are not drawn on layers if the
        // shape size and pos is the same as their hole:
        if( aSkipNPTHPadsWihNoCopper && (pad->GetAttribute() == PAD_ATTRIB_HOLE_NOT_PLATED) )
        {
            if( (pad->GetDrillSize() == pad->GetSize()) &&
                (pad->GetOffset() == wxPoint( 0, 0 )) )
            {
                switch( pad->GetShape() )
                {
                case PAD_SHAPE_CIRCLE:
                    if( pad->GetDrillShape() == PAD_DRILL_SHAPE_CIRCLE )
                        continue;
                    break;

                case PAD_SHAPE_OVAL:
                    if( pad->GetDrillShape() != PAD_DRILL_SHAPE_CIRCLE )
                        continue;
                    break;

                default:
                    break;
                }
            }
        }

        switch( aLayerId )
        {
        case F_Mask:
        case B_Mask:
            margin.x = margin.y = pad->GetSolderMaskMargin() + aInflateValue;
            break;

        case F_Paste:
        case B_Paste:
            margin = pad->GetSolderPasteMargin();
            margin.x += aInflateValue;
            margin.y += aInflateValue;
            break;

        default:
            margin.x = margin.y = aInflateValue;
            break;
        }

        createNewPad( pad, aDstContainer, margin );
    }
}

void TransformArcToSegments( const wxPoint &aCentre,
                             const wxPoint &aStart,
                             double aArcAngle,
                             int aCircleToSegmentsCount,
                             int aWidth,
                             CGENERICCONTAINER2D *aDstContainer,
                             const BOARD_ITEM &aBoardItem )
{
    wxPoint arc_start, arc_end;
    int     delta = 3600 / aCircleToSegmentsCount;   // rotate angle in 0.1 degree

    arc_end = arc_start = aStart;

    if( aArcAngle != 3600 )
    {
        RotatePoint( &arc_end, aCentre, -aArcAngle );
    }

    if( aArcAngle < 0 )
    {
        std::swap( arc_start, arc_end );
        aArcAngle = -aArcAngle;
    }

    // Compute the ends of segments and creates poly
    wxPoint curr_end    = arc_start;
    wxPoint curr_start  = arc_start;

    for( int ii = delta; ii < aArcAngle; ii += delta )
    {
        curr_end = arc_start;
        RotatePoint( &curr_end, aCentre, -ii );

        const SFVEC2F start3DU( curr_start.x * biu_to_3d_units, -curr_start.y * biu_to_3d_units );
        const SFVEC2F end3DU  ( curr_end.x   * biu_to_3d_units, -curr_end.y   * biu_to_3d_units );

        if( Is_segment_a_circle( start3DU, end3DU ) )
        {
            aDstContainer->Add( new CFILLEDCIRCLE2D( start3DU,
                                                     ( aWidth / 2 ) * biu_to_3d_units ,
                                                     aBoardItem ) );
        }
        else
        {
            aDstContainer->Add( new CROUNDSEGMENT2D( start3DU,
                                                     end3DU,
                                                     aWidth * biu_to_3d_units ,
                                                     aBoardItem ) );
        }

        curr_start = curr_end;
    }

    if( curr_end != arc_end )
    {
        const SFVEC2F start3DU( curr_end.x * biu_to_3d_units, -curr_end.y * biu_to_3d_units );
        const SFVEC2F end3DU  ( arc_end.x  * biu_to_3d_units, -arc_end.y  * biu_to_3d_units );

        if( Is_segment_a_circle( start3DU, end3DU ) )
        {
            aDstContainer->Add( new CFILLEDCIRCLE2D( start3DU,
                                                     ( aWidth / 2 ) * biu_to_3d_units,
                                                     aBoardItem ) );
        }
        else
        {
            aDstContainer->Add( new CROUNDSEGMENT2D( start3DU,
                                                     end3DU,
                                                     aWidth * biu_to_3d_units,
                                                     aBoardItem ) );
        }
    }
}

// Manual header redeclaration from `3d-viewer/3d_canvas/create_3Dgraphic_brd_items.cpp`
void addTextSegmToContainer( int x0, int y0, int xf, int yf, void* aData, int s_textWidth, CGENERICCONTAINER2D* s_dstcontainer, float s_biuTo3Dunits, const BOARD_ITEM* s_boardItem );

void AddShapeWithClearanceToContainer( const TEXTE_PCB* aTextPCB,
                                       CGENERICCONTAINER2D *aDstContainer,
                                       PCB_LAYER_ID aLayerId,
                                       int aClearanceValue )
{
    using namespace std::placeholders;  // for _1, _2, _3...

    wxSize size = aTextPCB->GetTextSize();

    if( aTextPCB->IsMirrored() )
        size.x = -size.x;

    const BOARD_ITEM* s_boardItem    = (const BOARD_ITEM *)&aTextPCB;
    CGENERICCONTAINER2D* s_dstcontainer = aDstContainer;
    int s_textWidth    = aTextPCB->GetThickness() + ( 2 * aClearanceValue );
    float s_biuTo3Dunits = biu_to_3d_units;

    // not actually used, but needed by DrawGraphicText
    const COLOR4D dummy_color = COLOR4D::BLACK;

    if( aTextPCB->IsMultilineAllowed() )
    {
        wxArrayString strings_list;
        wxStringSplit( aTextPCB->GetShownText(), strings_list, '\n' );
        std::vector<wxPoint> positions;
        positions.reserve( strings_list.Count() );
        aTextPCB->GetPositionsOfLinesOfMultilineText( positions,
                                                      strings_list.Count() );

        for( unsigned ii = 0; ii < strings_list.Count(); ++ii )
        {
            wxString txt = strings_list.Item( ii );

            DrawGraphicText( NULL, NULL, positions[ii], dummy_color,
                             txt, aTextPCB->GetTextAngle(), size,
                             aTextPCB->GetHorizJustify(), aTextPCB->GetVertJustify(),
                             aTextPCB->GetThickness(), aTextPCB->IsItalic(),
                             true, std::bind(addTextSegmToContainer, _1, _2, _3, _4, _5, s_textWidth, s_dstcontainer, s_biuTo3Dunits, s_boardItem) );
        }
    }
    else
    {
        DrawGraphicText( NULL, NULL, aTextPCB->GetTextPos(), dummy_color,
                         aTextPCB->GetShownText(), aTextPCB->GetTextAngle(), size,
                         aTextPCB->GetHorizJustify(), aTextPCB->GetVertJustify(),
                         aTextPCB->GetThickness(), aTextPCB->IsItalic(),
                         true, std::bind(addTextSegmToContainer, _1, _2, _3, _4, _5, s_textWidth, s_dstcontainer, s_biuTo3Dunits, s_boardItem) );
    }
}

void AddShapeWithClearanceToContainer( const DIMENSION* aDimension,
                                       CGENERICCONTAINER2D *aDstContainer,
                                       PCB_LAYER_ID aLayerId,
                                       int aClearanceValue )
{
    AddShapeWithClearanceToContainer(&aDimension->Text(), aDstContainer, aLayerId, aClearanceValue);

    const int linewidth = aDimension->GetWidth() + (2 * aClearanceValue);

    std::pair<wxPoint const *, wxPoint const *> segs[] = {
        {&aDimension->m_crossBarO,     &aDimension->m_crossBarF},
        {&aDimension->m_featureLineGO, &aDimension->m_featureLineGF},
        {&aDimension->m_featureLineDO, &aDimension->m_featureLineDF},
        {&aDimension->m_crossBarF,     &aDimension->m_arrowD1F},
        {&aDimension->m_crossBarF,     &aDimension->m_arrowD2F},
        {&aDimension->m_crossBarO,     &aDimension->m_arrowG1F},
        {&aDimension->m_crossBarO,     &aDimension->m_arrowG2F}};

    for( auto const & ii : segs )
    {
        const SFVEC2F start3DU(  ii.first->x * biu_to_3d_units,
                                -ii.first->y * biu_to_3d_units );

        const SFVEC2F end3DU  (  ii.second->x * biu_to_3d_units,
                                -ii.second->y * biu_to_3d_units );

        aDstContainer->Add( new CROUNDSEGMENT2D( start3DU,
                                                 end3DU,
                                                 linewidth * biu_to_3d_units,
                                                 *aDimension ) );
    }
}

void AddShapeWithClearanceToContainer( const DRAWSEGMENT* aDrawSegment,
                                       CGENERICCONTAINER2D *aDstContainer,
                                       PCB_LAYER_ID aLayerId,
                                       int aClearanceValue )
{
    // The full width of the lines to create
    // The extra 1 protects the inner/outer radius values from degeneracy
    const int linewidth = aDrawSegment->GetWidth() + (2 * aClearanceValue) + 1;

    switch( aDrawSegment->GetShape() )
    {
    case S_CIRCLE:
    {
        const SFVEC2F center3DU(  aDrawSegment->GetCenter().x * biu_to_3d_units,
                                 -aDrawSegment->GetCenter().y * biu_to_3d_units );

        const float inner_radius  =
                std::max<float>( (aDrawSegment->GetRadius() - linewidth / 2) * biu_to_3d_units, 0.0 );
        const float outter_radius = (aDrawSegment->GetRadius() + linewidth / 2) * biu_to_3d_units;

        aDstContainer->Add( new CRING2D( center3DU,
                                         inner_radius,
                                         outter_radius,
                                         *aDrawSegment ) );
    }
    break;

    case S_ARC:
    {
        const unsigned int nr_segments =
                GetNrSegmentsCircleBIU( aDrawSegment->GetBoundingBox().GetSizeMax() );

        TransformArcToSegments( aDrawSegment->GetCenter(),
                                aDrawSegment->GetArcStart(),
                                aDrawSegment->GetAngle(),
                                nr_segments,
                                aDrawSegment->GetWidth(),
                                aDstContainer,
                                *aDrawSegment );
    }
    break;

    case S_SEGMENT:
    {
        const SFVEC2F start3DU(  aDrawSegment->GetStart().x  * biu_to_3d_units,
                                -aDrawSegment->GetStart().y * biu_to_3d_units );

        const SFVEC2F end3DU  (  aDrawSegment->GetEnd().x    * biu_to_3d_units,
                                -aDrawSegment->GetEnd().y   * biu_to_3d_units );

        if( Is_segment_a_circle( start3DU, end3DU ) )
        {
            aDstContainer->Add( new CFILLEDCIRCLE2D( start3DU,
                                                     ( linewidth / 2 ) * biu_to_3d_units,
                                                     *aDrawSegment ) );
        }
        else
        {
            aDstContainer->Add( new CROUNDSEGMENT2D( start3DU,
                                                     end3DU,
                                                     linewidth * biu_to_3d_units,
                                                     *aDrawSegment ) );
        }
    }
    break;

    case S_CURVE:
    case S_POLYGON:
    {
        const int segcountforcircle = ARC_APPROX_SEGMENTS_COUNT_HIGH_DEF;
        const double correctionFactor = GetCircleCorrectionFactor( segcountforcircle );
        SHAPE_POLY_SET polyList;

        aDrawSegment->TransformShapeWithClearanceToPolygon( polyList, aClearanceValue,
                                                        segcountforcircle, correctionFactor );

        polyList.Simplify( SHAPE_POLY_SET::PM_FAST );

        if( polyList.IsEmpty() ) // Just for caution
            break;

        Convert_shape_line_polygon_to_triangles( polyList, *aDstContainer,
                                                 biu_to_3d_units, *aDrawSegment );
    }
    break;

    default:
        break;
    }
}

// TODO deduplicate with 3d-viewer/3d_canvas/create_layer_poly.cpp
void buildPadShapePolygon( const D_PAD* aPad,
                           SHAPE_POLY_SET& aCornerBuffer,
                           wxSize aInflateValue,
                           int aSegmentsPerCircle,
                           double aCorrectionFactor )
{
    wxPoint corners[4];
    wxPoint PadShapePos = aPad->ShapePos(); /* Note: for pad having a shape offset,
                                             * the pad position is NOT the shape position */
    switch( aPad->GetShape() )
    {
    case PAD_SHAPE_CIRCLE:
    case PAD_SHAPE_OVAL:
    case PAD_SHAPE_ROUNDRECT:
    {
        // We are using TransformShapeWithClearanceToPolygon to build the shape.
        // Currently, this method uses only the same inflate value for X and Y dirs.
        // so because here this is not the case, we use a inflated dummy pad to build
        // the polygonal shape
        // TODO: remove this dummy pad when TransformShapeWithClearanceToPolygon will use
        // a wxSize to inflate the pad size
        D_PAD dummy( *aPad );
        wxSize new_size = aPad->GetSize() + aInflateValue + aInflateValue;
        dummy.SetSize( new_size );
        dummy.TransformShapeWithClearanceToPolygon( aCornerBuffer, 0,
                                                    aSegmentsPerCircle, aCorrectionFactor );
    }
        break;

    case PAD_SHAPE_TRAPEZOID:
    case PAD_SHAPE_RECT:
    {
        SHAPE_LINE_CHAIN aLineChain;

        aPad->BuildPadPolygon( corners, aInflateValue, aPad->GetOrientation() );

        for( int ii = 0; ii < 4; ++ii )
        {
            corners[ii] += PadShapePos;          // Shift origin to position
            aLineChain.Append( corners[ii].x, corners[ii].y );
        }

        aLineChain.SetClosed( true );

        aCornerBuffer.AddOutline( aLineChain );
    }
        break;

    case PAD_SHAPE_CUSTOM:
        {
        SHAPE_POLY_SET polyList;     // Will contain the pad outlines in board coordinates
        polyList.Append( aPad->GetCustomShapeAsPolygon() );
        aPad->CustomShapeAsPolygonToBoardPosition( &polyList, aPad->ShapePos(), aPad->GetOrientation() );
        aCornerBuffer.Append( polyList );
        }
        break;
    }
}

// TODO deduplicate with 3d-viewer/3d_canvas/create_layer_poly.cpp
void transformPadsShapesWithClearanceToPolygon( const DLIST<D_PAD>& aPads, PCB_LAYER_ID aLayer,
                                                SHAPE_POLY_SET& aCornerBuffer,
                                                int aInflateValue,
                                                bool aSkipNPTHPadsWihNoCopper )
{
    wxSize margin;

    for( const D_PAD* pad = aPads; pad != NULL; pad = pad->Next() )
    {
        if( !pad->IsOnLayer(aLayer) )
            continue;

        // NPTH pads are not drawn on layers if the shape size and pos is the same
        // as their hole:
        if( aSkipNPTHPadsWihNoCopper && (pad->GetAttribute() == PAD_ATTRIB_HOLE_NOT_PLATED) )
        {
            if( (pad->GetDrillSize() == pad->GetSize()) &&
                (pad->GetOffset() == wxPoint( 0, 0 )) )
            {
                switch( pad->GetShape() )
                {
                case PAD_SHAPE_CIRCLE:
                    if( pad->GetDrillShape() == PAD_DRILL_SHAPE_CIRCLE )
                        continue;
                    break;

                case PAD_SHAPE_OVAL:
                    if( pad->GetDrillShape() != PAD_DRILL_SHAPE_CIRCLE )
                        continue;
                    break;

                default:
                    break;
                }
            }
        }

        switch( aLayer )
        {
        case F_Mask:
        case B_Mask:
            margin.x = margin.y = pad->GetSolderMaskMargin() + aInflateValue;
            break;

        case F_Paste:
        case B_Paste:
            margin = pad->GetSolderPasteMargin();
            margin.x += aInflateValue;
            margin.y += aInflateValue;
            break;

        default:
            margin.x = margin.y = aInflateValue;
            break;
        }

        unsigned int aCircleToSegmentsCount = GetNrSegmentsCircleBIU( pad->GetSize().x );
        double aCorrectionFactor = GetCircleCorrectionFactor( aCircleToSegmentsCount );

        buildPadShapePolygon( pad, aCornerBuffer, margin,
                                   aCircleToSegmentsCount, aCorrectionFactor );
    }
}

CLAYER_TRIANGLES* generate_3D_layer( const BOARD* board, const PCB_LAYER_ID layer_id ) {
    CBVHCONTAINER2D *container2d = new CBVHCONTAINER2D;

    SHAPE_POLY_SET *layerPoly = new SHAPE_POLY_SET;

    for( const TRACK* track = board->m_Track; track; track = track->Next() )
    {
        if( track->IsOnLayer( layer_id ) ) {
          container2d->Add( createNewTrack( track, 0.0f ) );

          // Add the track contour
          int nrSegments = GetNrSegmentsCircleBIU( track->GetWidth() );

          track->TransformShapeWithClearanceToPolygon(
                      *layerPoly,
                      0,
                      nrSegments,
                      GetCircleCorrectionFactor( nrSegments ) );
        }
    }

    // ADD PADS
    for( const MODULE* module = board->m_Modules; module; module = module->Next() )
    {
        // Note: NPTH pads are not drawn on copper layers when the pad
        // has same shape as its hole
        AddPadsShapesWithClearanceToContainer( module,
                                               container2d,
                                               layer_id,
                                               0,
                                               true );
        transformPadsShapesWithClearanceToPolygon( module->PadsList(),
                                                   layer_id,
                                                   *layerPoly,
                                                   0,
                                                   true );

        // Micro-wave modules may have items on copper layers
        /*AddGraphicsShapesWithClearanceToContainer( module,
                                                   layerContainer,
                                                   layer_id,
                                                   0 );
        module->TransformGraphicTextWithClearanceToPolygonSet( curr_layer_id,
                                                                *layerPoly,
                                                                0,
                                                                segcountforcircle,
                                                                correctionFactor );

        transformGraphicModuleEdgeToPolygonSet( module, layer_id, *layerPoly );*/
    }

    // ADD GRAPHIC ITEMS ON COPPER LAYERS (texts)
    for( const auto item : ((const BOARD*) board)->DrawingsConst() )
    {
        if( !item->IsOnLayer( layer_id ) )
            continue;

        switch( item->Type() )
        {
        case PCB_LINE_T:  // should not exist on copper layers
        {
            AddShapeWithClearanceToContainer( (DRAWSEGMENT*)item,
                                              container2d,
                                              layer_id,
                                              0 );
        }
        break;

        case PCB_TEXT_T:
            AddShapeWithClearanceToContainer( (TEXTE_PCB*) item,
                                              container2d,
                                              layer_id,
                                              0 );
        break;

        case PCB_DIMENSION_T:
            AddShapeWithClearanceToContainer( (DIMENSION*) item,
                                              container2d,
                                              layer_id,
                                              0 );
        break;

        default:
        break;
        }
    }

    const LIST_OBJECT2D &listObject2d = container2d->GetList();

    float layer_z_bot = -copperThickness3DU/2;
    float layer_z_top = copperThickness3DU/2;

    // Calculate an estimation for the nr of triangles based on the nr of objects
    unsigned int nrTrianglesEstimation = listObject2d.size() * 8;

    CLAYER_TRIANGLES *layerTriangles = new CLAYER_TRIANGLES( nrTrianglesEstimation );

    // Load the 2D (X,Y axis) component of shapes
    for( LIST_OBJECT2D::const_iterator itemOnLayer = listObject2d.begin();
         itemOnLayer != listObject2d.end();
         ++itemOnLayer )
    {
        const COBJECT2D *object2d_A = static_cast<const COBJECT2D *>(*itemOnLayer);

        switch( object2d_A->GetObjectType() )
        {
        case OBJ2D_FILLED_CIRCLE:
            add_object_to_triangle_layer( (const CFILLEDCIRCLE2D *)object2d_A,
                                          layerTriangles, layer_z_top, layer_z_bot );
            break;

        case OBJ2D_POLYGON4PT:
            add_object_to_triangle_layer( (const CPOLYGON4PTS2D *)object2d_A,
                                          layerTriangles, layer_z_top, layer_z_bot );
            break;

        case OBJ2D_RING:
            add_object_to_triangle_layer( (const CRING2D *)object2d_A,
                                          layerTriangles, layer_z_top, layer_z_bot );
            break;

        case OBJ2D_TRIANGLE:
            add_object_to_triangle_layer( (const CTRIANGLE2D *)object2d_A,
                                          layerTriangles, layer_z_top, layer_z_bot );
            break;

        case OBJ2D_ROUNDSEG:
            add_object_to_triangle_layer( (const CROUNDSEGMENT2D *) object2d_A,
                                          layerTriangles, layer_z_top, layer_z_bot );
            break;

        default:
            wxFAIL_MSG("C3D_RENDER_OGL_LEGACY: Object type is not implemented");
            break;
        }
    }

    // Load the vertical (Z axis)  component of shapes
    if( layerPoly->OutlineCount() > 0 )
        layerTriangles->AddToMiddleContourns( *layerPoly, layer_z_bot, layer_z_top,
                                              biu_to_3d_units, false );

    return layerTriangles;
}
