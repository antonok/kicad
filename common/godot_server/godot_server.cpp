
#define BOOST_BEAST_ALLOW_DEPRECATED

#include <godot_server.h>
#include <json.hpp>

#include <boost/optional.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>

#include <algorithm>
#include <functional>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <sstream>

namespace beast = boost::beast;
namespace http = beast::http;
namespace websocket = beast::websocket;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;
using json = nlohmann::json;

// report a failure
void fail( boost::beast::error_code ec, char const* what )
{
    std::cerr << what << ": " << ec.message() << "\n";
}

/**
 * Represents a connection session with a single client
 */
class GODOT_SESSION : public std::enable_shared_from_this<GODOT_SESSION>
{
private:
    std::shared_ptr<GODOT_SERVER>               server_;
    websocket::stream<tcp::socket>              ws_;
    beast::multi_buffer                         rx_buffer_;
    beast::multi_buffer                         tx_buffer_;
    std::function<void()> on_close_cb_;
    std::function<void(std::shared_ptr<GODOT_SESSION>, std::string)> on_receive_cb_;

public:
    // Currently echos back all recieved WebSocket messages

    // Take ownership of the socket
    explicit GODOT_SESSION( std::shared_ptr<GODOT_SERVER> server, tcp::socket&& socket )
            : server_( std::move( server ) ),
              ws_( std::move( socket ) ),
              rx_buffer_(),
              tx_buffer_(),
              on_close_cb_(),
              on_receive_cb_()
    {
        std::cout << "[ new client ]" << std::endl;
    }

    ~GODOT_SESSION()
    {
        std::cout << "[ end client ]" << std::endl;
        if( on_close_cb_ )
            on_close_cb_();
    }

    // start the asynchronous operation, with callback for onclose and onreceieve
    void run( std::function<void()> on_close, std::function<void(std::shared_ptr<GODOT_SESSION>, std::string)> on_receive )
    {
        // TODO(mike) timeouts
        // TODO(mike) decorator to change server of the handshake

        // accept the client handshake (bind_executor puts us on the right thread)
        // TODO this probably shouldn't fire if we can't open the connection or something
        on_close_cb_ = on_close;
        on_receive_cb_ = on_receive;
        ws_.async_accept( beast::bind_front_handler(
                &GODOT_SESSION::on_accept,
                shared_from_this() ) );
    }

    void run()
    {
        run( nullptr, nullptr );
    }

    // accepting the client's handshake
    void on_accept( beast::error_code ec )
    {
        if( ec )
            return fail( ec, "accept" );
        // read a message
        do_read();
    }

    void do_read()
    {
        std::cerr << "do read" << std::endl;
        // read a message into our buffer
        ws_.async_read( rx_buffer_, beast::bind_front_handler(
                 &GODOT_SESSION::on_read, shared_from_this() ) );
    }

    void on_read( beast::error_code ec, std::size_t bytes_transferred )
    {

        // prevent compiler warnings
        boost::ignore_unused( bytes_transferred );

        if( ec == websocket::error::closed ) {
            std::cerr << "websocket error closed" << std::endl;
            return; // session closed
        }
        if( ec == net::error::eof ) {
            std::cerr << "net error eof" << std::endl;
            return; // end of stream
        }

        if( ec )
        {
            std::cerr << "other ec" << std::endl;
            fail( ec, "read" );
            // TODO(mike) need to delete this somehow???
            ws_.close( "error" ); // TODO(mike) maybe don't close on all errors?
            return;
        }

        std::cerr << "no errors" << std::endl;

        // get a string from the buffer
        std::string smsg = beast::buffers_to_string(rx_buffer_.data());

        // call receive cb
        if (on_receive_cb_) on_receive_cb_(shared_from_this(), beast::buffers_to_string(rx_buffer_.data()));

        // don't need the buffer contents anymore
        rx_buffer_.consume( rx_buffer_.size() );

        // Clear the buffer and wait for the next message
        do_read();
    }

    void send( const json& msg )
    {
        ws_.text( true );
        // TODO(mike) use tx_buffer_ for this
        ws_.async_write( net::buffer( msg.dump() ),
                beast::bind_front_handler(
                        &GODOT_SESSION::on_write, shared_from_this() ) );
    }

    void on_write( beast::error_code ec, std::size_t bytes_transferred )
    {
        std::cout << "TX\t" << beast::buffers( tx_buffer_.data() ) << std::endl;
        boost::ignore_unused( bytes_transferred );

        // consume rest of buffer
        tx_buffer_.consume( tx_buffer_.size() );

        if( ec )
        {
            fail( ec, "write" );
            ws_.close( "error" ); // TODO(mike) maybe don't close on all errors?
            return;
        }
    }
};

/* GODOT_SERVER definitions */

boost::optional<std::string> GODOT_SERVER::GetGLTFOutputDir() const {
    return outDir_;
}

void GODOT_SERVER::Send( const json& msg )
{
    for( auto sessionptr : open_sessions_ )
    {
        if( !sessionptr.expired() )
        {
            auto sp = sessionptr.lock();
            sp->send( msg );
        }
    }
}

void GODOT_SERVER::SendUpdate(const PCB_LAYER_ID layer_id, const std::string& filename) {
    // TODO this interface is in flux. This should probably at least also send vias.
    Send({
        {"type", "update-gltf-layer"}, 
        {"payload", {
            {"layer_id", filename}
        }}
    });
}

void GODOT_SERVER::run_forever()
{

    // start listening for the first connection
    // the callback for this calls do_accept again, completing the
    // reconnection loop
    do_accept();

    ioc_.run(); // TODO(mike) what does this do?
}

GODOT_SERVER::GODOT_SERVER( const std::string& ip, const unsigned short port )
        : outDir_(),
          ioc_( 1 ),
          acceptor_( ioc_ ),
          socket_( ioc_ ),
          open_sessions_lock_(),
          open_sessions_() // TODO(mike) this socket feels weird
{
    auto const    address = boost::asio::ip::make_address( ip );
    tcp::endpoint endpoint( address, port );

    // accept incoming connections and launch a session for each one.

    boost::beast::error_code ec;

    // open the acceptor
    acceptor_.open( endpoint.protocol(), ec );
    if( ec )
    {
        fail( ec, "open" );
        exit( 1 ); // TODO(mike) fail more gracefully
    }

    // allow address reuse (?)
    acceptor_.set_option( boost::asio::socket_base::reuse_address( true ), ec );
    if( ec )
    {
        fail( ec, "set_option" );
        exit( 1 ); // TODO(mike) fail more gracefully
    }

    // bind to server address
    acceptor_.bind( endpoint, ec );
    if( ec )
    {
        fail( ec, "bind" );
        exit( 1 ); // TODO(mike) fail more gracefully
    }

    // start listening for connections
    acceptor_.listen( boost::asio::socket_base::max_listen_connections, ec );
    if( ec )
    {
        fail( ec, "listen" );
        exit( 1 ); // TODO(mike) fail more gracefully
    }
}

void GODOT_SERVER::do_accept()
{
    acceptor_.async_accept( socket_,                                 // puts us on the right thread
            std::bind( &GODOT_SERVER::on_accept, shared_from_this(), // handler
                    std::placeholders::_1 ) );
}

// called on a new connection
void GODOT_SERVER::on_accept( boost::beast::error_code ec )
{
    if( ec )
        fail( ec, "accept" );
    else
    {
        // create a session and run it

        auto sessionptr =
                std::make_shared<GODOT_SESSION>( shared_from_this(), std::move( socket_ ) );

        std::lock_guard<std::mutex> lock( open_sessions_lock_ );

        sessionptr->run( nullptr, std::bind( &GODOT_SERVER::handle_message, shared_from_this(), std::placeholders::_1, std::placeholders::_2 ));

        open_sessions_.push_back( sessionptr );
    }

    // accept another connection
    do_accept();
}

void GODOT_SERVER::handle_message(std::shared_ptr<GODOT_SESSION> session, std::string smsg) {

    // convert to msg
    json msg;
    try {
        msg = json::parse(smsg);
    } catch (const json::parse_error& e) {
        std::cerr << "Error parsing message '" << smsg.substr(10) << "': " << e.what() << std::endl;
        return;
    }
    
    // TODO(mike) remove this?
    std::cerr << "RX\t" << msg << std::endl;

    // confirm message has type and payload
    if (!(msg.contains("type") && msg.contains("payload"))) {
        std::cerr << "Invalid message format: " << msg << std::endl;
        return;
    }
    
    json type = msg["type"];
    json payload = msg["payload"];

    if (type == "setup") {
        if (payload.contains("dumplocation")) {

            if (payload["dumplocation"].is_string()) {
            
                std::string dump = payload["dumplocation"].get<std::string>();

                if (true) { // TODO check that its a directory
                    outDir_ = dump;
                    std::cout << "SET DIR:\t" << *outDir_ << std::endl;
                } else {
                    std::cerr << "dumplocation not a directory: " << dump << std::endl;
                }
            } else {
                std::cerr << "Bad dumplocation " << payload["dumplocation"] << std::endl;
            }
        }
    } else if (type == "debug") {
        std::cout << "DEBUG:\t" <<  payload << std::endl;
    } else {
        std::cerr << "BAD MESSAGE: " << payload << std::endl;
    }
}

void GODOT_SERVER::on_close( GODOT_SESSION&& s )
{
    return;
}
