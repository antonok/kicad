#include "godot_server.h"

#include <iostream>

int main(int argc, char** argv)
{
    // create server
    auto server = std::make_shared<GODOT_SERVER>("127.0.0.1", 8080);

    std::cout << "running" << std::endl;
    server->run_forever();

    return EXIT_SUCCESS;

}
